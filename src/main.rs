use std::collections::HashMap;

#[derive(Debug, Clone)]
struct Directory {
    pub size: u32,
    pub subdirs: Vec<String>,
}

impl Directory {
    pub fn with_size(size: u32) -> Self {
        Self {
            size,
            subdirs: Vec::<String>::new(),
        }
    }

    pub fn with_subdir(subdir: String) -> Self {
        Self {
            size: 0,
            subdirs: vec![subdir],
        }
    }

    pub fn add_subdir(&mut self, subdir: String) {
        self.subdirs.push(subdir);
    }
}

fn main() {
    let input = include_str!("../input.txt");
    println!("Solution A: {}", solve_for_a(input));
    println!("Solution B: {}", solve_for_b(input));
}

fn handle_cd(line: &str, pwd: &mut String) {
    if let Some(cd_argument) = line.get(5..line.len()) {
        match cd_argument {
            "/" => {
                *pwd = String::from("/");
            }
            ".." => {
                let offset = pwd.rfind('/').unwrap_or(pwd.len());
                pwd.replace_range(offset..pwd.len(), "");
                // pwd.drain(offset..pwd.len()); // any difference?
            }
            _ => {
                if pwd != "/" {
                    pwd.push('/');
                }
                pwd.push_str(cd_argument)
            }
        };
    }
}

fn build_horrible_data_structure(input: &str) -> HashMap<String, Directory> {
    let mut directories = HashMap::<String, Directory>::new();
    let mut pwd: String = String::from("/");

    for line in input.lines() {
        match line.get(0..4) {
            Some("$ cd") => handle_cd(line, &mut pwd),
            Some("$ ls") => {} // no-op
            Some("dir ") => {
                if let Some(subdir_name) = line.get(4..line.len()) {
                    let mut subdir_path = pwd.clone();
                    if pwd != "/" {
                        subdir_path.push('/');
                    }
                    subdir_path.push_str(subdir_name);
                    directories
                        .entry(pwd.clone())
                        .and_modify(|e| e.add_subdir(subdir_path.clone()))
                        .or_insert_with(|| Directory::with_subdir(subdir_path));
                };
            }
            _ => {
                let size = line
                    .split_ascii_whitespace()
                    .next()
                    .unwrap()
                    .parse::<u32>()
                    .unwrap();

                directories
                    .entry(pwd.clone())
                    .and_modify(|e| e.size += size)
                    .or_insert_with(|| Directory::with_size(size));
            }
        };
    }

    directories
}

fn sum_subdirs(directories: &mut HashMap<String, Directory>) {
    let mut directory_paths: Vec<_> = directories.keys().cloned().collect();
    directory_paths.sort();
    directory_paths.sort_by_key(|a| a.matches('/').count());
    directory_paths.reverse();

    for directory_path in directory_paths {
        let mut subdirs_size = 0;
        if let Some(directory) = &directories.get(&directory_path) {
            if directory.subdirs.is_empty() {
                continue;
            }
            for subdir_path in &directory.subdirs {
                if let Some(subdir) = directories.get(subdir_path) {
                    subdirs_size += subdir.size;
                }
            }
            directories
                .entry(directory_path)
                .and_modify(|e| e.size += subdirs_size);
        }
    }
}

fn solve_for_a(input: &str) -> u32 {
    let mut directories = build_horrible_data_structure(input);
    sum_subdirs(&mut directories);

    let purgable_dirs_total_size = directories
        .iter()
        .filter(|(_, dir)| dir.size <= 100000)
        .fold(0, |acc, (_, dir)| acc + dir.size);

    purgable_dirs_total_size
}

fn solve_for_b(input: &str) -> u32 {
    let mut directories = build_horrible_data_structure(input);
    sum_subdirs(&mut directories);

    const TOTAL_DISK_SIZE: u32 = 70000000;
    const DISK_SPACE_REQUIRED: u32 = 30000000;
    let total_space_in_use = directories.get("/").unwrap().size;
    let space_deficit = DISK_SPACE_REQUIRED - (TOTAL_DISK_SIZE - total_space_in_use);

    let mut candidates = directories
        .iter()
        .filter(|(_, dir)| dir.size >= space_deficit)
        .collect::<Vec<_>>();
    candidates.sort_by_key(|(_, dir)| dir.size);
    candidates.reverse();
    candidates.pop().unwrap().1.size
}

#[test]
fn test_solve_for_a() {
    let input = include_str!("../test.txt");
    assert_eq!(95437, solve_for_a(input));
}

#[test]
fn test_solve_for_b() {
    let input = include_str!("../test.txt");
    assert_eq!(24933642, solve_for_b(input));
}
